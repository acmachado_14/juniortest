<?php

namespace Tests\Unit;

use App\Models\Mensagem;
use App\Models\User;
use App\Observers\MensagemObserver;
use DateTime;
use PHPUnit\Framework\TestCase;

class FromArrayTest extends TestCase
{
    public function testFunctionFromArray()
    {
        $array = [
            'mensagem' => 'teste',
            'id_user' => 1,
            'sender' => 'Alan Turing',
            'topic' => 'teste'
        ];

        $mensagem = new Mensagem();
        $array = $mensagem->fromArray($array);

        $this->assertInstanceOf('App\Models\Mensagem', $array);
    }
}