<?php

namespace Tests\Functional;

use App\Models\Mensagem;
use App\Models\User;
use App\Observers\MensagemObserver;
use DateTime;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;


class MensagemObserverFTest extends TestCase
{

    private $user;

    public function setUp(): void
    {
        parent::setUp();
        $user = User::factory()->make();
        $user->save();
        $this->user = $user;
    }

    public function testCanSaveMensagem(): void
    {
        $response = $this->post('http://127.0.0.1:8000/mensagem/criar', [
            'mensagem' => 'teste',
            'id_user' => $this->user->id,
            'sender' => 'Alun Turing',
            'topic' => 'test'
        ]);

        $response->assertStatus(200);

        $this->assertDatabaseHas('mensagens', [
            'mensagem' => 'teste',
            'id_user' => $this->user->id,
            'sender' => 'Alun Turing',
            'topic' => 'test',
        ]);
    }

    public function testCanObserveMensagem(): void
    {
        $response = $this->get('http://127.0.0.1:8000/mensagem/ver');
        $response->assertStatus(200);
    }
}

