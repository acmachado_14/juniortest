<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Mensagem extends Model
{
    use HasFactory;

    protected $table = 'mensagens';
    protected $fillable = [
        'mensagem',
        'id_user',
        'sender',
        'topic'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function fromArray(array $data): self
    {
        $mensagem = new self();
        $mensagem->mensagem = $data['mensagem'];
        $mensagem->id_user = $data['id_user'];
        $mensagem->sender = $data['sender'];
        $mensagem->topic = $data['topic'];

        return $mensagem;
    }
}
