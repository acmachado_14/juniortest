<?php

namespace App\Services;

use App\Models\Mensagem;
use App\Models\User;
use Nexmo\Laravel\Facade\Nexmo;

class AcoesAoReceberMensgens
{
    private $user;
    private $mensagem;

    public function __construct(Mensagem $mensagem)
    {
        $this->user = User::find($mensagem->id_user);
        $this->mensagem = $mensagem;
    }

    public function executar(): void
    {
        //basta criar a funcao e colocar ela aqui:
        //$this->mandarSmS();
        $this->notificarGenerico();
    }

    private function mandarSmS(): void
    {
        Nexmo::message()->send([
            'to'   => $this->user->number,
            'from' => '5531986268630',
            'text' => 'Chegou email novo pra voce pratao'
        ]);
    }

    private function notificarGenerico(): void
    {
        //aqui é onde configura a mensagem
        echo "Nova Mensagem para voce! De {$this->mensagem->sender}:";
        echo "<br>";
        echo $this->mensagem->mensagem;
    }

}