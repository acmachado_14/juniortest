<?php

namespace App\Observers;

use App\Models\Mensagem;
use App\Services\AcoesAoReceberMensgens;

class MensagemObserver
{
    public $afterCommit = true;
    /**
     * Handle the Mensagem "created" event.
     *
     * @param  \App\Models\Mensagem  $mensagem
     * @return void
     */
    public function created(Mensagem $mensagem)
    {
        $men = new AcoesAoReceberMensgens($mensagem);
        $men->executar();
    }

    /**
     * Handle the Mensagem "updated" event.
     *
     * @param  \App\Models\Mensagem  $mensagem
     * @return void
     */
    public function updated(Mensagem $mensagem)
    {
        //
    }

    /**
     * Handle the Mensagem "deleted" event.
     *
     * @param  \App\Models\Mensagem  $mensagem
     * @return void
     */
    public function deleted(Mensagem $mensagem)
    {
        //
    }

    /**
     * Handle the Mensagem "restored" event.
     *
     * @param  \App\Models\Mensagem  $mensagem
     * @return void
     */
    public function restored(Mensagem $mensagem)
    {
        //
    }

    /**
     * Handle the Mensagem "force deleted" event.
     *
     * @param  \App\Models\Mensagem  $mensagem
     * @return void
     */
    public function forceDeleted(Mensagem $mensagem)
    {
        //
    }
}
