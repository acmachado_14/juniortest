<?php

namespace App\Http\Controllers;

use App\Models\Mensagem;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MensagemController extends Controller
{
    public function storeMensagem(Request $request): void
    {
        $mensagemData = [
            'mensagem' => $request->mensagem,
            'id_user' => $request->id_user,
            'sender' => $request->sender,
            'topic' => $request->topic
        ];

        $mensagem = Mensagem::fromArray($mensagemData);

        DB::beginTransaction();
        try {
            $mensagem->save();
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollback();
        }

    }

    public function getStoredMensagem()
    {
        $user = User::factory()->make();
        $user->save();

        $mensagemData = [
            'mensagem' =>  'teste',
            'id_user' =>  $user->id,
            'sender' =>  'teste',
            'topic' =>  'teste'
        ];

        $mensagem = Mensagem::fromArray($mensagemData);

        DB::beginTransaction();
        try {
            $mensagem->save();
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollback();
        }

    }

}
