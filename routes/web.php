<?php

use App\Http\Controllers\MensagemController;
use App\Http\Controllers\SmsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::post('/mensagem/criar', [MensagemController::class, 'storeMensagem']);

Route::get('/mensagem/ver', [MensagemController::class, 'getStoredMensagem']);