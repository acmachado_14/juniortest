## Sobre
Projeto feito com o framework Laravel 8, PHP e MySQL

## Requisitos
- Composer
- Docker e Docker-compose
- PHP 8.0^

## Como Executar
```
composer install
```
Aletere as variáveis do banco do arquivo .env com o padrao configurado no docker-compose.yml

```
DB_CONNECTION=mysql
DB_HOST=localhost
DB_PORT=3306
DB_DATABASE=dbjuniorTest
DB_USERNAME=root
DB_PASSWORD=juniorTest
```
Execute na pasta raiz do projeto
```
docker-compose up
```
Rode as migrations e preecha o banco
```
php artisan migrate && php artisan db:seed
```

## Testes

Execute na pasta raiz do projeto
```
vendor/bin/phpunit
```
